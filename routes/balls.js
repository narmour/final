var express = require('express');
var router = express.Router();
var balls_dal = require('../model/balls_dal');
var company_dal = require('../model/companies_dal');
var balls_dal = require('../model/balls_dal');


// View All ballss
router.get('/all', function(req, res) {
    balls_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {

            res.render('balls/ballsViewAll', { 'result':result });

        }
    });

});


// View the company for the given id
router.get('/', function(req, res){
    if(req.query.bowler_id == null) {
        res.send('bowler_id is null');
    }
    else {
        balls_dal.getById(req.query.bowler_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                console.log(result[0][0].name);
                res.render('balls/ballsList', {'result': result[0]});
            }
        });
    }
});


// Return the add a new balls form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    company_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('balls/ballsAdd', {'company': result,});


        }
    });
});


router.get('/insert', function(req, res){
    // simple validation
    var x = req.query;
    if(x.name == null || x.cname == null||x.coverstock == null || x.color ==null) {
        res.send('all info must be provided ! ! ! !');
    }

    else {
        balls_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                balls_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('balls/ballsViewAll', { 'result':result,'was_successful':true});
                    }
                });

            }
        });
    }
});

module.exports = router;
