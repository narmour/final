var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'PBA Bowlers and Balls' });
});



router.get('/about', function(req, res, next) {
    res.render('about', { title: 'PBA Bowlers and Balls' });
});

module.exports = router;
