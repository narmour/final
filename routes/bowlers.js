var express = require('express');
var router = express.Router();
var bowlers_dal = require('../model/bowlers_dal');
var company_dal = require('../model/companies_dal');
var balls_dal = require('../model/balls_dal');


// View All bowlerss
router.get('/all', function(req, res) {
    bowlers_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            //console.log(result);
            res.render('bowlers/bowlersViewAll', { 'result':result });

        }
    });

});

// View All bowlerss
router.get('/filterView', function(req, res) {
    console.log("got to filter view");
    bowlers_dal.avgFilter(req.query.avt,function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            //console.log(result);
            res.render('bowlers/bowlersViewAll', { 'result':result });

        }
    });

});


// Return the add a new bowlers form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    company_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            c = result;
            balls_dal.getAll(function(err,result){
                if(err){
                    res.send(err);
                }
                else{
                    res.render('bowlers/bowlersAdd', {'company': c,'balls':result});
                }
            })

        }
    });
});


// View the account for the given id
router.get('/insert', function(req, res){
    // simple validation
    var x = req.query;
    if(x.first == null || x.last == null||x.age == null || x.avg ==null
    || x.earnings == null || x.titles == null || x.cname ==null) {
        res.send('all info must be provided ! ! ! !');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        bowlers_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                bowlers_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('bowlers/bowlersViewAll', { 'result':result,'was_successful':true});
                    }
                });

            }
        });
    }
});

module.exports = router;
