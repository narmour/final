var express = require('express');
var router = express.Router();
var companies_dal = require('../model/companies_dal');
var company_dal = require('../model/companies_dal');


// View All companiess
router.get('/all', function(req, res) {
    companies_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {

            res.render('companies/companiesViewAll', { 'result':result });

        }
    });

});


// Return the add a new companies form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    company_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('companies/companiesAdd', {'company': result,});


        }
    });
});


router.get('/insert', function(req, res){
    // simple validation
    var x = req.query;
    if(x.name == null || x.cname == null||x.coverstock == null || x.color ==null) {
        res.send('all info must be provided ! ! ! !');
    }

    else {
        companies_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                companies_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('companies/companiesViewAll', { 'result':result,'was_successful':true});
                    }
                });

            }
        });
    }
});

module.exports = router;
