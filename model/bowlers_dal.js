var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM ProfessionalBowlers;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO ProfessionalBowlers (age,fname,lname,main_sponsor,season_avg,' +
        'l_or_r_handed,season_earnings,num_titles) ' +
        'VALUES (?,?,?,?,?,?,?,?)';
    var queryData = [params.age,params.first,params.last,params.cname,params.avg,
    params.lr,params.earnings,params.titles];
    //console.log(params.bname[0]);

    connection.query(query, queryData, function(err, result) {
        var bowler_id = result.insertId;
        //console.log(bowler_id);
        var query = 'INSERT INTO BallLookup(bowler_id,ball_name) values (?)';
        var ballData = [];
        params.bname.forEach(function(b){
            x = [bowler_id,b];
            ballData.push(x);
        })
        // cant get the bulk inserts to work. sorry haderman
        ballData.forEach(function(q){
            connection.query(query,[q],function(err,result){
            })

        });

        callback(err,result);
    });



};

exports.avgFilter = function(avg,callback){
    var query = 'select * from ProfessionalBowlers pb where pb.bowler_id in(select bowler_id from ProfessionalBowlers having season_avg > ?)';
    var queryData = [avg];
    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
    }