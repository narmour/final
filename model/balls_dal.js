var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM BowlingBalls;';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};



exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO BowlingBalls (name,company_name,coverstock,color ) VALUES(?,?,?,?)';
    var queryData = [params.name,params.cname,params.coverstock,params.color];
    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });



};



exports.getById = function(bowler_id, callback) {
    var query = 'call get_pro_balls(?)';
    var queryData = [bowler_id];

    connection.query(query, queryData, function(err, result) {
        //console.log(result);
        callback(err, result);
    });
};

